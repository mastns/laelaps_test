#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include <inttypes.h>
#include <errno.h>
#include <error.h>

#include "dm7820_library.h"

#include "ros/ros.h"

//#include "nodes/LegEncoders.h"

#include <time.h>
#include <sys/timeb.h>  /* ftime, timeb (for timestamp in millisecond) */
#include <sys/time.h>   /* gettimeofday, timeval (for timestamp in microsecond) */
//#include "nodes/Timer.h"
//#include <ros/time.h>
//#include "nodes/LegCommand.h"
//#include "nodes/MotionControl.h"
//#include "nodes/ControlChoice.h"
//#include "nodes/InitFlag.h"
//#include "nodes/Interrupter.h"
//#include "nodes/PWM_drive.h"
//#include "pid/plant_msg.h"
//#include "pid/controller_msg.h"
//#include <nodes/ControlConfig.h>
//#include <dynamic_reconfigure/server.h>
#include <std_msgs/Float64.h>
//#include "nodes/timeandvalue.h"
//
#include <sstream>
//
#define PWM_GEN_PERIOD 1000
//
#define DIR_A         0x0020
#define DIR_B         0x0010
#define DIR_C         0x1000
#define DIR_D         0x2000
//
#define INH_B         0x0040
#define INH_A         0x0080
#define INH_C         0x8000
#define INH_D         0x4000
//
#define GEAR_RATIO    53
#define SATURATION_POS 10*GEAR_RATIO
#define LOOP_RATE     300
//
std_msgs::Float64 fb_A_msg;
std_msgs::Float64 fb_B_msg;
std_msgs::Float64 fb_C_msg;
std_msgs::Float64 fb_D_msg; // Feedback message for topic feedback_leg
//
//
uint16_t DIRstatusA1 = 0;
uint16_t DIRstatusB1 = 0;
uint16_t DIRstatusC1 = 0;
uint16_t DIRstatusD1 = 0;
//
uint16_t DIRstatusA2 = 0;
uint16_t DIRstatusB2 = 0;
uint16_t DIRstatusC2 = 0;
uint16_t DIRstatusD2 = 0;
//
//
static int16_t pwm_periodA = 0;
static int16_t pwm_periodB = 0;
static int16_t pwm_periodC = 0;
static int16_t pwm_periodD = 0;
// Init Pwm Values
static uint16_t pwm_dutyA = 0;
static uint16_t pwm_dutyB = 0;
static uint16_t pwm_dutyC = 0;
static uint16_t pwm_dutyD = 0;
//
int8_t control_start_choice  = 0;
//
int minor_number = 0;
double time_span = 0;
double time_pre = 0;
//
DM7820_Board_Descriptor *io_board;  // Digital I/O board descriptor and status variables
DM7820_Error dm7820_status;
//
// *************************************************** Callback functions ****************************************************
//
void signalHandler1(int signal)
{

    //const char *signal_name;

    switch (signal) {
        
        case 10:
            DIRstatusA1 = DIR_A;
            
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 12:
            DIRstatusB1 = DIR_B;
            
            //printf(" \n SIGUSR2 \n");
            break;
        case 14:
            DIRstatusC1 = DIR_C;
            
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 15:
            DIRstatusD1 = DIR_D;
           
            //ROS_INFO(" \n SIGUSR2 \n");
            break;
        case 17:
            DIRstatusA2 = DIR_A;
            //signal_name = "SIGCHLD";
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 18:
            DIRstatusB2 = DIR_B;
            //signal_name = "SIGCONT";
            //ROS_INFO(" \n SIGUSR2 \n");
            break;
        case 21:
            DIRstatusC2 = DIR_C;
            //signal_name = "SIGSTOP";
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 20:
            DIRstatusD2 = DIR_D;
            //signal_name = "SIGTSTP";
            //ROS_INFO(" \n SIGUSR2 \n");
            break;
        default:
            //ROS_INFO(stderr, "Caught wrong signal: %d\n", signal);
            return;
    }
    fflush(stdout);
}
//
void signalHandler2(int signal)
{

    //const char *signal_name;

    switch (signal) {
        
        case 22:
            //DIRstatusA1 |= DIR_A;
            DIRstatusA1 = 64;
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 23:
            DIRstatusB1 = 64;
            
            //printf(" \n SIGUSR2 \n");
            break;
        case 24:
            DIRstatusC1 = 8192;
            
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 25:
            DIRstatusD1 = 16384;
            
            //ROS_INFO(" \n SIGUSR2 \n");
            break;
        case 26:
            DIRstatusA2 = 64;
            //signal_name = "SIGCHLD";
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 27:
            DIRstatusB2 = 64;
            //signal_name = "SIGCONT";
            //ROS_INFO(" \n SIGUSR2 \n");
            break;
        case 4:
            DIRstatusC2 = 8192;
            //signal_name = "SIGSTOP";
            //ROS_INFO("\n SIGUSR1 \n");
            break;
        case 29:
            DIRstatusD2 = 16384;
            //signal_name = "SIGTSTP";
            //ROS_INFO(" \n SIGUSR2 \n");
            break;
        default:
            //ROS_INFO(stderr, "Caught wrong signal: %d\n", signal);
            return;
    }
    fflush(stdout);
}
//
int msleep(unsigned long milisec)   
{   
    struct timespec req={0};   
    time_t sec=(int)(milisec/1000);   
    milisec=milisec-(sec*1000);   
    req.tv_sec=sec;   
    req.tv_nsec=milisec*1000000L;   
    while(nanosleep(&req,&req)==-1)   
        continue;   
    return 1;   
}
//
// *************************************************************************************************************************
// Main Function
int main(int argc, char **argv)
{
  // Initialize ROS node
  //argv[0] = "Module_interface100";
  ros::init(argc, argv, "interface1");
  ros::NodeHandle n;

  signal(10, signalHandler1);
  signal(12, signalHandler1);
  signal(14, signalHandler1);
  signal(15, signalHandler1);
  //
  signal(17, signalHandler1);
  signal(18, signalHandler1);
  signal(21, signalHandler1);
  signal(20, signalHandler1);
  //
  signal(22, signalHandler2);
  signal(23, signalHandler2);
  signal(24, signalHandler2);
  signal(25, signalHandler2);
  //
  signal(26, signalHandler2);
  signal(27, signalHandler2);
  signal(4, signalHandler2);
  signal(29, signalHandler2);
  //
  int msg_count = 0;
  //
  // Get board Number from Launch File
  // Launch File sets DM9820 board ( 0 is the one closer to CPU, 3  is the one further away)
  ros::param::get("~board_number", minor_number);
  ROS_INFO("Opening device with minor number %u ...\n",minor_number);
  dm7820_status = DM7820_General_Open_Board(minor_number, &io_board);
  DM7820_Return_Status(dm7820_status, "DM7820_General_Open_Board()");
  ROS_INFO("board: %d   ", (int) minor_number );
  //
  ROS_INFO("My Module interface PID is: %d", getpid());
  //
  // Init Encoder Values
  uint16_t value_0_a = 0;
  uint16_t value_0_b = 0;
  uint16_t value_1_a = 0;
  uint16_t value_1_b = 0;
  //
  // Direction
  
  uint16_t input_value = 0;
  int16_t tmp_enc[4] = {0,0,0,0};
  float t_IC = 0.0000;
  //
  //pid::plant_msg  passive_enc_1_a, passive_enc_1_b;
  dm7820_incenc_phase_filter phase_filter;
  //
  // Topics 
  //ros::Publisher bit_pub = n.advertise<nodes::Interrupter>("interrupt_bits", 1000);
  //
  ros::Publisher pid_A_pub = n.advertise<std_msgs::Float64>("feedback_A", 1000);
  ros::Publisher pid_B_pub = n.advertise<std_msgs::Float64>("feedback_B", 1000);
  ros::Publisher pid_C_pub = n.advertise<std_msgs::Float64>("feedback_C", 1000);
  ros::Publisher pid_D_pub = n.advertise<std_msgs::Float64>("feedback_D", 1000);
  //
  ros::Rate loop_rate(LOOP_RATE);
  //

  /*
   * Disable incremental encoder 0 to put it into a known state; any
   * incremental encoder should be disabled before programming it
   *
   */

  fprintf(stdout, "Disabling incremental encoder 0 ...\n");

  dm7820_status = DM7820_IncEnc_Enable(io_board, DM7820_INCENC_ENCODER_0, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Enable()");
  dm7820_status = DM7820_IncEnc_Enable(io_board, DM7820_INCENC_ENCODER_1, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Enable()");

  dm7820_status = DM7820_StdIO_Set_IO_Mode(io_board, DM7820_STDIO_PORT_0, 0xFFFF, DM7820_STDIO_MODE_INPUT);
  DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_IO_Mode()");
  dm7820_status = DM7820_StdIO_Set_IO_Mode(io_board, DM7820_STDIO_PORT_1, 0xFFFF, DM7820_STDIO_MODE_INPUT);
  DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_IO_Mode()");
  
  //ROS_INFO("Initializing incremental encoders...\n");

  //ROS_INFO("Setting master clock ...\n");
  dm7820_status = DM7820_IncEnc_Set_Master(io_board, DM7820_INCENC_ENCODER_0, DM7820_INCENC_MASTER_25_MHZ);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Set_Master()");
  dm7820_status = DM7820_IncEnc_Set_Master(io_board, DM7820_INCENC_ENCODER_1, DM7820_INCENC_MASTER_25_MHZ);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Set_Master()");

  //ROS_INFO("Disabling value register hold ...\n");
  dm7820_status = DM7820_IncEnc_Enable_Hold(io_board, DM7820_INCENC_ENCODER_0, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Enable_Hold()");
  dm7820_status = DM7820_IncEnc_Enable_Hold(io_board, DM7820_INCENC_ENCODER_1, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Enable_Hold()");

  //ROS_INFO("Configuring encoders...\n");

  DM7820_INCENC_RESET_PHASE_FILTER(phase_filter);
  
  dm7820_status = DM7820_IncEnc_Configure(io_board, DM7820_INCENC_ENCODER_0, phase_filter, DM7820_INCENC_INPUT_SINGLE_ENDED, 0x00, DM7820_INCENC_CHANNEL_INDEPENDENT, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Configure()");
  dm7820_status = DM7820_IncEnc_Configure(io_board, DM7820_INCENC_ENCODER_1, phase_filter, DM7820_INCENC_INPUT_SINGLE_ENDED, 0x00, DM7820_INCENC_CHANNEL_INDEPENDENT, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Configure()");

  //ROS_INFO("Setting initial value for channels...\n");
  dm7820_status = DM7820_IncEnc_Set_Independent_Value(io_board, DM7820_INCENC_ENCODER_0, DM7820_INCENC_CHANNEL_A, 0x8000);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Set_Independent_Value()");
  dm7820_status = DM7820_IncEnc_Set_Independent_Value(io_board, DM7820_INCENC_ENCODER_0, DM7820_INCENC_CHANNEL_B, 0x8000); // 0x6401 actual initial angle of second link
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Set_Independent_Value()");
  dm7820_status = DM7820_IncEnc_Set_Independent_Value(io_board, DM7820_INCENC_ENCODER_1, DM7820_INCENC_CHANNEL_A, 0x8000);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Set_Independent_Value()");
  dm7820_status = DM7820_IncEnc_Set_Independent_Value(io_board, DM7820_INCENC_ENCODER_1, DM7820_INCENC_CHANNEL_B, 0x8000);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Set_Independent_Value()");
  
  // ROS_INFO("Enabling incremental encoders ...\n");
  dm7820_status = DM7820_IncEnc_Enable(io_board, DM7820_INCENC_ENCODER_0, 0xFF);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Enable()");
  dm7820_status = DM7820_IncEnc_Enable(io_board, DM7820_INCENC_ENCODER_1, 0xFF);
  DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Enable()");
  
  // ROS_INFO("Disabling pulse width modulators ...\n");
  dm7820_status = DM7820_PWM_Enable(io_board, DM7820_PWM_MODULATOR_0, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Enable()");
  dm7820_status = DM7820_PWM_Enable(io_board, DM7820_PWM_MODULATOR_1, 0x00);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Enable()");
  // ROS_INFO("Initializing digital I/O port 2 ...\n");
  // ROS_INFO("Setting bits to peripheral output ...\n");
  dm7820_status = DM7820_StdIO_Set_IO_Mode(io_board, DM7820_STDIO_PORT_2, 0xFFFF, DM7820_STDIO_MODE_PER_OUT);
  DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_IO_Mode()");

  // ROS_INFO("Setting bits to output PWM peripheral ...\n");
  dm7820_status = DM7820_StdIO_Set_Periph_Mode(io_board, DM7820_STDIO_PORT_2, 0xFFFF, DM7820_STDIO_PERIPH_PWM);
  DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_Periph_Mode()");

  // Initializing pulse width modulator 0 ******************************
  // ROS_INFO("Initializing pulse width modulator 0 ...\n");

  // fprintf(stdout, "    Setting period master clock ...\n");

  dm7820_status = DM7820_PWM_Set_Period_Master(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_PERIOD_MASTER_25_MHZ);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Period_Master()");

  // ROS_INFO("Setting period to ~20 KHz...\n");
  dm7820_status = DM7820_PWM_Set_Period(io_board, DM7820_PWM_MODULATOR_0, 1200);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Period()");
  // ROS_INFO("Setting width master clock ...\n");
  dm7820_status = DM7820_PWM_Set_Width_Master(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_WIDTH_MASTER_25_MHZ);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width_Master()");
 
  // Initializing pulse width modulator 1 ******************************
  // ROS_INFO("Initializing pulse width modulator 1 ...\n");
  //
  /*
   * Set period master clock to 25 MHz clock
   */

  // fprintf(stdout, "    Setting period master clock ...\n");
  dm7820_status = DM7820_PWM_Set_Period_Master(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_PERIOD_MASTER_25_MHZ);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Period_Master()");

  // ROS_INFO("Setting period to ~20 KHz...\n");
  dm7820_status = DM7820_PWM_Set_Period(io_board, DM7820_PWM_MODULATOR_1, 1200);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Period()");
  // ROS_INFO("Setting width master clock ...\n");
  dm7820_status = DM7820_PWM_Set_Width_Master(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_WIDTH_MASTER_25_MHZ);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width_Master()");

  // Enable PWM
  // fprintf(stdout, "    Enabling PWM ...\n");
  dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_OUTPUT_A, 0);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
  dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_OUTPUT_B, 0);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
  dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_OUTPUT_A, 0);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
  dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_OUTPUT_B, 0);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
  dm7820_status = DM7820_PWM_Enable(io_board, DM7820_PWM_MODULATOR_0, 0xFF);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Enable()");
  dm7820_status = DM7820_PWM_Enable(io_board, DM7820_PWM_MODULATOR_1, 0xFF);
  DM7820_Return_Status(dm7820_status, "DM7820_PWM_Enable()");

  //ROS_INFO("Starting communication with interface module.");
  
  //ROS_INFO("Setting direction bits to output ...\n");
  //
  // Enbale Direction and Inhibit Pins
  dm7820_status = DM7820_StdIO_Set_IO_Mode(io_board, DM7820_STDIO_PORT_2, (DIR_A | DIR_B) | (DIR_C | DIR_D) | (INH_A | INH_B) | (INH_C | INH_D), DM7820_STDIO_MODE_OUTPUT);
  DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_IO_Mode()");

  struct timeb timer_msec;
  long int timestamp_msec; /* timestamp in millisecond. */
  double seconds = 0.0;
  time_pre = seconds;
  int c_char = 0;

  ROS_INFO("Entering ROS OK \n");

  while (ros::ok()){
        // Get encoder values
        dm7820_status = DM7820_IncEnc_Get_Independent_Value(io_board, DM7820_INCENC_ENCODER_0, DM7820_INCENC_CHANNEL_A, &value_0_a);
        DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Get_Independent_Value()");
        dm7820_status = DM7820_IncEnc_Get_Independent_Value(io_board, DM7820_INCENC_ENCODER_0, DM7820_INCENC_CHANNEL_B, &value_0_b);
        DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Get_Independent_Value()");
        dm7820_status = DM7820_IncEnc_Get_Independent_Value(io_board, DM7820_INCENC_ENCODER_1, DM7820_INCENC_CHANNEL_A, &value_1_a);
        DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Get_Independent_Value()");
        dm7820_status = DM7820_IncEnc_Get_Independent_Value(io_board, DM7820_INCENC_ENCODER_1, DM7820_INCENC_CHANNEL_B, &value_1_b);
        DM7820_Return_Status(dm7820_status, "DM7820_IncEnc_Get_Independent_Value()");
        //
        
        // Get encoder values  and assign them to PID feedback message
        //
        if (!ftime(&timer_msec)) {
          timestamp_msec = ((long long int) timer_msec.time) * 1000ll + (long long int) timer_msec.millitm;
        }
        else {
          timestamp_msec = -1;
        }
        seconds = (double) timestamp_msec / 1000.0;
        //
        // Get encoder values  and assign them to PID feedback message
        fb_A_msg.data = -1*(float)(value_0_a - 0x8000);  //the minus sign compensates the reverse power supply voltage at the terminals of the motors
        //
        fb_B_msg.data = -1*(float)(value_0_b - 0x8000);  //the minus sign compensates the reverse power supply voltage at the terminals of the motors
        //
        fb_C_msg.data = -1*(float)(value_1_a - 0x8000);  //the minus sign compensates the reverse power supply voltage at the terminals of the motors
        //  
        fb_D_msg.data = -1*(float)(value_1_b - 0x8000);  //the minus sign compensates the reverse power supply voltage at the terminals of the motors
        //
        /*
         * Read from input port
         */
        dm7820_status = DM7820_StdIO_Get_Input(io_board, DM7820_STDIO_PORT_0, &input_value);
        DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Get_Input()");
        //
        msg_count++;
        if(msg_count == 4000)
        {
          msg_count = 0;
          
        }
      
        if (msg_count == 1500 ){
                  
                  ROS_INFO("pwm_dutyA : %d \n", (int) pwm_dutyA);
                  ROS_INFO("pwm_dutyB : %d \n", (int) pwm_dutyB);
                  ROS_INFO("pwm_dutyC : %d \n", (int) pwm_dutyC);
                  ROS_INFO("pwm_dutyD : %d \n", (int) pwm_dutyD);
                  //
                  ROS_INFO("pwm_periodA : %d \n", (int) pwm_periodA);
                  ROS_INFO("pwm_periodB : %d \n", (int) pwm_periodB);
                  ROS_INFO("pwm_periodC : %d \n", (int) pwm_periodC);
                  ROS_INFO("pwm_periodD : %d \n", (int) pwm_periodD);
      			     //
                  ROS_INFO("Ros Time -- : %f \n", seconds);

                  if (minor_number == 3){ 
                  //
                   ROS_INFO("My Module interface Board A PID is: %d", getpid());
                   ROS_INFO("DIR_A1 -- : %d \n", DIRstatusA1);
                   ROS_INFO("DIR_B1 -- : %d \n", DIRstatusB1);
                   ROS_INFO("DIR_C1 -- : %d \n", DIRstatusC1);
                   ROS_INFO("DIR_D1 -- : %d \n", DIRstatusD1);
                  //
                  }
                  //
                  if (minor_number == 2){
                  //
                    ROS_INFO("My Module interface Board B PID is: %d", getpid());
                    ROS_INFO("DIR_A2 -- : %d \n", DIRstatusA2);
                    ROS_INFO("DIR_B2 -- : %d \n", DIRstatusB2);
                    ROS_INFO("DIR_C2 -- : %d \n", DIRstatusC2);
                    ROS_INFO("DIR_D2 -- : %d \n", DIRstatusD2);
                  //
                  }
        }
        //
        seconds = (double) timestamp_msec / 1000.0;
        time_span = time_span + (seconds - time_pre);
        time_pre = seconds;
        //
        if (minor_number == 3){ 
        //
           dm7820_status = DM7820_StdIO_Set_Output(io_board, DM7820_STDIO_PORT_2, DIRstatusA1 | DIRstatusB1 | DIRstatusC1 | DIRstatusD1 | INH_A | INH_B | INH_C | INH_D ); //set DIR & set KNEE inhibit high 
        // 
        }
        if (minor_number == 2){
        //
           dm7820_status = DM7820_StdIO_Set_Output(io_board, DM7820_STDIO_PORT_2, DIRstatusA2 | DIRstatusB2 | DIRstatusC2 | DIRstatusD2 | INH_A | INH_B | INH_C | INH_D ); //set DIR & set KNEE inhibit high 
        //
        }
        pwm_dutyA = 500; //
        pwm_dutyB = 500;
        pwm_dutyC = 500;
        pwm_dutyD = 500;

        DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_Output()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_OUTPUT_A, pwm_dutyA);  //set PWM  
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_OUTPUT_B, pwm_dutyB);  //set PWM
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_OUTPUT_A, pwm_dutyC);  //set PWM
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_OUTPUT_B, pwm_dutyD);  //set PWM
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");      
        //
        pid_A_pub.publish(fb_A_msg);  // publish feedback encoder value (PID node msg)
        pid_B_pub.publish(fb_B_msg);  // publish feedback encoder value (PID node msg)
        //
        pid_C_pub.publish(fb_C_msg);
        pid_D_pub.publish(fb_D_msg);
        //
        ros::spinOnce();
        loop_rate.sleep();
  }
  //
  ROS_INFO("Termination... \n");
  // Set signals DIR PWM INH to ouput pins
  int i_sleep = 0;
  for (i_sleep = 1; i_sleep = 10; i_sleep++) { 
        msleep(1000); // sleep for 1000ms = 1 sec 
        pwm_dutyA = pwm_dutyA / 2;
        pwm_dutyB = pwm_dutyB / 2;
        pwm_dutyC = pwm_dutyC / 2;
        pwm_dutyD = pwm_dutyD / 2;
        //
        DM7820_Return_Status(dm7820_status, "DM7820_StdIO_Set_Output()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_OUTPUT_A, pwm_dutyA);  //set PWM  
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_0, DM7820_PWM_OUTPUT_B, pwm_dutyB);  //set PWM
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_OUTPUT_A, pwm_dutyC);  //set PWM
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");
        dm7820_status = DM7820_PWM_Set_Width(io_board, DM7820_PWM_MODULATOR_1, DM7820_PWM_OUTPUT_B, pwm_dutyD);  //set PWM
        DM7820_Return_Status(dm7820_status, "DM7820_PWM_Set_Width()");      
    }
  //
  return 0;
}
